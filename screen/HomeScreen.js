import React, { Component } from "react";
import{ View, Text, StyleSheet, ImageBackground, ScrollView, Image, Button, TouchableOpacity} from "react-native";
import { Header,Left,Icon } from "native-base";

class Home extends Component{

    static navigationOptions = {
        drawerIcon: ({ tintColor }) => (
            <Icon name="home" style={{fontSize:24, color: tintColor}} />
        )
    }

    render(){
        return(
            <ImageBackground source={require('../image/mountain.png')} style={styles.container}>
                
                <View style={styles.head}>
                    <Header style={{height:78, backgroundColor:"#99ffff"}}>
                        <Left>
                            <Icon style={{position:'absolute', left:-80}} name="menu" onPress={()=>this.props.navigation.openDrawer()}/>
                        </Left>
                    </Header>
                </View>
                <ScrollView style={styles.inner} scrollEventThrottle={16}>
                    <View style={styles.container}>
                        
                        <Text style={{fontSize:22, fontWeight:'700', paddingHorizontal:20, marginTop:15}}>
                            Know Thy People
                        </Text>
                       
                        <Text style={{marginTop:30, marginLeft:15, marginRight:15, marginStart:25, fontSize:14}}>
                            With over 7,600 islands in the Philippines and three major island groups, it’s no wonder 
                            that different cultural practices, traditions, and groups are present in the country. Among 
                            the archipelago’s existing communities, there are indigenous tribes who have managed to keep
                            their cultural identity, despite the non-recognition and marginalization they’re facing.
                        </Text>

                        <Image style={{width:280, height:190, borderWidth:20, marginTop:20}} source={require('../image/indigenous-1.jpg')}></Image>

                        <Text style={{marginTop:30, marginLeft:15, marginRight:15, marginStart:25, fontSize:14}}>
                            Although there are quite a number of indigenous tribes or ethnic groups in the country, they 
                            remain some of the most poor, least privileged, and impeded members of society. They mostly 
                            reside in the mountains, and hence were not affected by Spanish or American colonization, which 
                            is the primary reason they were able to retain their customs and traditions.  
                        </Text>
                        <Text style={{marginTop:20, marginLeft:15, marginRight:15, marginStart:25, fontSize:14, marginBottom:70}}>
                            There are two main ethnic groups comprising several upland and lowland indigenous tribes living 
                            within the Philippines – from the northern and southern parts of the Philippines. The indigenous 
                            eople living in the northern part of the country are called the Igorots, whereas those non-Muslim 
                            indigenous tribes living in the south are referred to as Lumad. 
                        </Text>

                        <Text style={{fontSize:20, paddingHorizontal:20}}>
                            Northern Igorots          
                        </Text>
                        
                        <TouchableOpacity  onPress={() => this.props.navigation.navigate('Igorot')}>
                            <Image style={{width:287, height:190, borderWidth:20,marginTop:15}} source={require('../image/igorot-2.jpg')}></Image>
                        </TouchableOpacity>
                        
                        <Text style={{fontSize:20, paddingHorizontal:20, marginTop:35}}>
                            Southern Lumads       
                        </Text>
                        
                        <TouchableOpacity  onPress={() => this.props.navigation.navigate('Lumad')}>
                            <Image style={{width:287, height:190, borderWidth:20,marginTop:15}} source={require('../image/lumad-2.jpg')}></Image>
                        </TouchableOpacity>

                        <Text style={{fontSize:20, paddingHorizontal:20, marginTop:35}}>
                            Other Major Tribes           
                        </Text>
                        
                        <TouchableOpacity  onPress={() => this.props.navigation.navigate('Tribes')}>
                            <Image style={{width:287, height:190, borderWidth:20,marginTop:15}} source={require('../image/other.jpg')}></Image>
                        </TouchableOpacity>



                        <Text style={{marginBottom:40}}></Text>
                    </View>
                </ScrollView>


            </ImageBackground>
        );
    }
}


const styles=StyleSheet.create({
    container: {
        flex:1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    inner: {
        width: '94%',
        height: '99%',
        backgroundColor: 'rgba(255,255,255, 0.7)'
    },
    head:{
        width:'100%',
        height:'15%'
    }

});

export default Home;