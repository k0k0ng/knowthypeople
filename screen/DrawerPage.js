import React, { Component } from 'react';
import { Text, View, HeaderBackButton, StyleSheet } from "react-native";
import { createDrawerNavigator } from "react-navigation";

import  HomeScreen from "./HomeScreen";
import  Igorot from "./Igorot";
import  Lumad  from "./Lumad";


const Drawer = new createDrawerNavigator({
    HomeScreen: {screen: HomeScreen},
    Igorot: {screen: Igorot},
    Lumad: {screen: Lumad}
})

export default Drawer;