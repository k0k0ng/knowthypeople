import React, { Component } from "react";
import{ View, Text, StyleSheet, ImageBackground, ScrollView, Image} from "react-native";

import {createBottomTabNavigator} from 'react-navigation'
import { Header,Left,Icon } from "native-base";

class Lumad extends Component{

    static navigationOptions = {
        drawerIcon: ({ tintColor }) => (
            <Icon name="people" style={{fontSize:24, color: tintColor}} />
        )
    }
    
    render(){
        return(
            <ImageBackground source={require('../image/lumad-3.jpg')} style={styles.container}>
                <View style={styles.head}>
                    <Header style={{height:78, backgroundColor:"#99ffff"}}>
                        <Left>
                            <Icon style={{position:'absolute', left:-80}} name="menu" onPress={()=>this.props.navigation.openDrawer()}/>
                        </Left>
                    </Header>
                </View>
                <ScrollView style={styles.inner} scrollEventThrottle={16}>
                    
                <View style={styles.container}>
                        
                        <Text style={{fontSize:22, fontWeight:'700', paddingHorizontal:20}}>
                            Southern Lumads           
                        </Text>
                        <Text style={{fontSize:18, fontWeight:'400', paddingHorizontal:20}}>
                            of the      
                        </Text>
                        <Text style={{fontSize:22, fontWeight:'700', paddingHorizontal:20}}>
                            Philippine's                
                        </Text>

                        <Image style={{width:230, height:150, borderWidth:20, marginTop:20}} source={require('../image/lumad-1.jpg')}></Image>

                        <Text style={{marginTop:30, marginLeft:15, marginRight:15, fontSize:14}}>
                            Meanwhile, in the southern part of the country, indigenous tribes are mostly found in 
                            Mindanao and Western Visayas. In Mindanao, these existing non-Muslim indigenous groups are 
                            collectively known as the Lumad – a Cebuano term which means ‘native’ or ‘indigenous’. 
                            There Lumad tribes comprise about 13 ethnic groups which are the Blaan, Bukidnon, Higaonon, 
                            Mamanwa, Mandaya, Manobo, Mansaka, Sangir, Subanen, Tagabawa, Tagakaulo, Tasaday, and T’boli. 
                            Their tribe is generally known for tribal music produced by musical instruments they’ve created.      
                        </Text>

                        <Text style={{marginTop:20, marginLeft:15, marginRight:15, marginBottom:55, fontSize:14}}>
                            Among those mentioned above, the Manobo tribe includes further big ethnic groups such as 
                            the Ata-Manobo, Agusan-Manobo, and Dulangan-Manobo to name a few. The total population of 
                            the Manobo group is unknown as they occupy core areas in main provinces of the Mindanao 
                            Region.      
                        </Text>

                    </View>
                </ScrollView>
            </ImageBackground>
        );
    }
}

class Products extends Component{
    render(){
        return(
            <ImageBackground source={require('../image/Products/lumad-product-3.jpg')} style={styles.container}>
                <View style={styles.head}>
                    <Header style={{height:78, backgroundColor:"#99ffff"}}>
                        <Left>
                            <Icon style={{position:'absolute', left:-80}} name="menu" onPress={()=>this.props.navigation.openDrawer()}/>
                        </Left>
                    </Header>
                </View>
                <ScrollView style={styles.inner} scrollEventThrottle={16}>
                    
                    <View style={styles.container}>
                        
                        <Text style={{fontSize:22, fontWeight:'500', paddingHorizontal:20}}>
                            Southern Lumads      
                        </Text>
                        <Text style={{fontSize:22, fontWeight:'500', paddingHorizontal:20}}>
                            Product's               
                        </Text>

                        <Text style={{fontSize:14, paddingHorizontal:20, marginTop:30}}>- Amulets</Text>
                        <Image style={{width:230, height:150, borderWidth:20, marginTop:10}} source={require('../image/Products/lumad-product-1.jpg')}></Image>

                        <Text style={{fontSize:14, paddingHorizontal:20, marginTop:25}}>- Accessories</Text>
                        <Image style={{width:230, height:150, borderWidth:20, marginTop:10}} source={require('../image/Products/lumad-product-2.jpg')}></Image>

                        <Text style={{fontSize:14, paddingHorizontal:20, marginTop:25}}>- Hand made Bracelets</Text>
                        <Image style={{width:230, height:450, borderWidth:20, marginTop:10, marginBottom:40}} source={require('../image/Products/lumad-product-3.jpg')}></Image>

                    </View>
                </ScrollView>
            </ImageBackground>
        );
    }
} 


const HomeScreenTabNavigator = createBottomTabNavigator({
    Lumad: {screen: Lumad,
        navigationOptions:{
            tabBarLabel: 'About',
            tabBarIcon: ({ tintColor }) => (
               <Icon name="people" style={{fontSize:24, color: tintColor}} />
            )
        }},
    Products: {screen: Products,
        navigationOptions:{
            tabBarLabel: 'Products and Activities',
            tabBarIcon: ({ tintColor }) => (
               <Icon name="bicycle" style={{fontSize:24, color: tintColor}} />
            )
        }}
},{
    animationEnabled: true
});


const styles=StyleSheet.create({
    container: {
        flex:1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    inner: {
        width: '94%',
        height: '99%',
        backgroundColor: 'rgba(255,255,255, 0.7)',
        marginTop:10
    },
    head:{
        width:'100%',
        height:'15%'
    }

});

export default HomeScreenTabNavigator;