import React, { Component } from 'react';
import { StyleSheet, ViewPagerAndroid, View, Text, Image, ScrollView, ImageBackground } from "react-native";
import { Header,Left,Icon } from "native-base";

class Tribes extends Component {

    render() {
        return (
            
          <ViewPagerAndroid
            style={styles.viewPager}
            initialPage={0}>
            
            <View style={styles.pageStyle} key="1">
            <ImageBackground source={require('../image/other.jpg')} style={{width:'130%',height:"100%"}}>
                <View style={styles.head}>
                    <Header style={{height:78, backgroundColor:"#99ffff"}}>
                        <Left>
                            <Icon style={{position:'absolute', left:-60}} name="menu" onPress={()=>this.props.navigation.openDrawer()}/>
                        </Left>
                    </Header>
                </View>
                <ScrollView style={styles.inner}>
                    
                <View style={styles.container}>
                        
                        <Text style={{fontSize:22, fontWeight:'700', paddingHorizontal:20, marginTop:35}}>
                            Other major Tribes            
                        </Text>
                        <Text style={{fontSize:18, fontWeight:'400', paddingHorizontal:20}}>
                            in the      
                        </Text>
                        <Text style={{fontSize:22, fontWeight:'700', paddingHorizontal:20}}>
                            Philippine's                 
                        </Text>

                        <Text style={{marginTop:30, marginLeft:60, marginRight:60, fontSize:14}}>
                            Apart from the two main indigenous groups mentioned above, the following tribes have also 
                            kept their customs and traditions.  
                        </Text>
                        <Text style={{marginTop:25, marginLeft:60, marginRight:60, fontSize:14}}>
                            Today, the indigenous groups in the country remain in their original ancestral lands as they 
                            preserve their cultural practices and traditions. And while the Philippine government passed 
                            the Indigenous Peoples’ Rights Act of 1997, there are still issues left unsolved, including 
                            their right for inclusion.   
                        </Text>
                        <Icon name="md-arrow-dropleft" style={{fontSize:60, marginTop:50, marginRight:200}} />
                        <Text style={{fontSize:18, fontWeight:'400', marginTop:-45}}>   
                             Swipe to navigate 
                        </Text>
                        <Icon name="md-arrow-dropright" style={{fontSize:60, marginTop:-42, marginLeft:200}} />

                    </View>
                </ScrollView>
            </ImageBackground>
            </View>

            <View style={styles.pageStyle} key="2">
            <ImageBackground source={require('../image/badjao-3.jpg')} style={styles.container}>
                
                <ScrollView style={styles.inner} scrollEventThrottle={16}>
                    
                <View style={styles.container}>
                        
                        <Text style={{fontSize:22, fontWeight:'100', paddingHorizontal:20, marginTop:40}}>
                            Badjao              
                        </Text>

                        <Image style={{width:230, height:150, borderWidth:20, marginTop:25}} source={require('../image/badjao-3.jpg')}></Image>

                        <Text style={{marginTop:25, marginLeft:15, marginRight:15, fontSize:14}}>
                            Widely known as the “Sea Gypsies” of the Sulu and Celebes Seas, the Badjao are scattered 
                            along the coastal areas of Tawi Tawi, Sulu, Basilan, and some coastal municipalities of 
                            Zamboanga del Sur in the ARMM. Amongst themselves, they're known as Sama Laus (Sea Sama) 
                            and are found living on houseboats where they make their livelihood solely on the sea as 
                            expert fishermen, deep sea divers, and navigators. 
                        </Text>
                        <Image style={{width:230, height:150, borderWidth:20, marginTop:15}} source={require('../image/badjao-2.jpg')}></Image>
                        <Text style={{marginTop:25, marginLeft:15, marginRight:15, fontSize:14}}> 
                            They come to shore to barter their 
                            harvests for farmed produce such as fruits and cassava, as well as, replenish their supplies 
                            and/or make repairs to their houseboats. Unique to their cultural rituals is the concept of 
                            life and their relationship to the sea: For example, as a childbirth ritual, a newly born 
                            infant is thrown into the sea and members of the clan dive to save the newborn. Other 
                            traditions such as marriages are prearranged by the parents for their sons and daughters; 
                            the process similar to other ethnic groups, in that, a dowry is often presented to the 
                            parents of the woman a man wishes to marry. And, only the Badjao leader can consecrate a 
                            marriage. Therefore a leader is chosen based on individual inherent virtues, wisdom, and 
                            “charisma”...an inate ability to attract followers.   
                        </Text>
                        <Image style={{width:230, height:150, borderWidth:20, marginTop:25}} source={require('../image/badjao-1.jpg')}></Image>
                        <Text style={{marginTop:25, marginLeft:15, marginRight:15, fontSize:14}}>
                            Originally from the islands of Sulu in Mindanao, they’re known as the sea tribes living 
                            on houseboats. They try to make ends meet by depending on the sea as divers, fishermen, 
                            and navigators. Because of conflicts in the region, the majority of them has migrated to 
                            neighboring countries such as Malaysia and Indonesia, whereas those who stayed in the 
                            Philippines moved to some areas in Luzon.    
                        </Text>
                        <Text style={{marginTop:40, marginLeft:0, fontSize:14}}>
                            - Daily activities and source of income   
                        </Text>
                        <Image style={{width:230, height:150, borderWidth:20, marginTop:15}} source={require('../image/Products/badjao-product-2.jpg')}></Image>
                        <Image style={{width:230, height:150, borderWidth:20, marginTop:15, marginBottom:30}} source={require('../image/Products/badjao-product-3.jpg')}></Image>

                    </View>
                </ScrollView>
            </ImageBackground>
            </View>

            <View style={styles.pageStyle} key="3">
            <ImageBackground source={require('../image/ati-2.jpg')} style={styles.container}>
                
                <ScrollView style={styles.inner} scrollEventThrottle={16}>
                    
                <View style={styles.container}>
                        
                        <Text style={{fontSize:22, fontWeight:'100', paddingHorizontal:20, marginTop:40}}>
                            Ati and Tumandok               
                        </Text>

                        <Image style={{width:230, height:150, borderWidth:20, marginTop:25}} source={require('../image/ati-2.jpg')}></Image>

                        <Text style={{marginTop:30, marginLeft:15, marginRight:15, fontSize:14}}>
                            One of the few clans in Visayas, the Ati and Tumandok tribes of Panay Island are the 
                            first to call the island their home. Genetically related to other indigenous groups in the 
                            country, they mostly resemble the Aetas or Negritos who are characterised by their dark skin. 
                            While some adopted Western religions, they still carry some animistic beliefs and rituals 
                            passed down by their ancestors.   
                        </Text>
                        <Image style={{width:230, height:150, borderWidth:20, marginTop:15}} source={require('../image/ati-1.jpg')}></Image>
                        <Text style={{marginTop:25, marginLeft:15, marginRight:15, fontSize:14}}>
                            Atis cultivate a variety of crops for their livelihood. Tobacco is bartered for the products 
                            of their Visayan neighbors. During September and October, they work at the sugar plantations 
                            of Christian landowners. Other means of subsistence are hunting, fishing, handicrafts, and 
                            bow-and-arrow making; working as household help and midwives; and practicing herbal medicine 
                            as herbolarios.  
                        </Text>
                        <Image style={{width:230, height:150, borderWidth:20, marginTop:15}} source={require('../image/ati-3.jpg')}></Image>
                        <Text style={{marginTop:40, marginLeft:0, fontSize:14}}>
                            - Daily activities and source of income   
                        </Text>
                        <Image style={{width:230, height:150, borderWidth:20, marginTop:15, marginBottom:30}} source={require('../image/Products/ati-product-1.jpg')}></Image>



                    </View>
                </ScrollView>
            </ImageBackground>
            </View>

            <View style={styles.pageStyle} key="4">
            <ImageBackground source={require('../image/palawenos-1.jpg')} style={styles.container}>
                
                <ScrollView style={styles.inner} scrollEventThrottle={16}>
                    
                <View style={styles.container}>
                        
                        <Text style={{fontSize:22, fontWeight:'100', paddingHorizontal:20, marginTop:40}}>
                            Palawan Tribes              
                        </Text>

                        <Image style={{width:230, height:150, borderWidth:20, marginTop:25}} source={require('../image/palawenos-1.jpg')}></Image>

                        <Text style={{marginTop:30, marginLeft:15, marginRight:15, fontSize:14}}>
                            Palawan is also home to various tribes such as the Batak, Palaweño, Palawano, and the 
                            Tagbanwa. Mostly living in mountains or lowland dwellings, some of these groups have also 
                            been included in the large Manobo tribe of the South. They have not totally embraced urban 
                            living, with the majority living in more rural settings.      
                        </Text>

                        <Text style={{fontSize:15, fontWeight:'200', paddingHorizontal:20, marginTop:35}}>
                            Batak                          
                        </Text>

                        <Image style={{width:230, height:150, borderWidth:20, marginTop:15}} source={require('../image/batak-3.png')}></Image>

                        <Text style={{marginTop:30, marginLeft:15, marginRight:15, fontSize:14}}>
                            In Palawan, the smallest and the most endangered of the three major ethnic groups is the 
                            Batak tribe. An old Cuyunon term, Batak means “mountain people”; they are classified as a 
                            Philippine Aeta group because of their physical characteristics. At present, they live in 
                            the rugged northeastern part of Palawan Island, close to the coastal villages of Babuyan, 
                            Tinitian, and Malcampo. Previously, they lived in several river valleys of Babuyan, Maoyon, 
                            Tanabag, Tarabanan, Laingogan, Tagnipa, Caramay, and Buayan. They speak a language called 
                            Batak or Binatak, although majority are bilingual, as they can speak both Batak and Tagbanua.
                            Bataks practice minimal shifting cultivation, alternating rice with cassava, tubers, and 
                            vegetables.     
                        </Text>

                        <Text style={{fontSize:15, fontWeight:'200', paddingHorizontal:20, marginTop:35}}>
                            Palaweños                          
                        </Text>

                        <Image style={{width:230, height:150, borderWidth:20, marginTop:15}} source={require('../image/palawenos-2.jpg')}></Image>

                        <Text style={{marginTop:30, marginLeft:15, marginRight:15, fontSize:14}}>
                            The island of Cuyo between Palawan and Panay islands is the home of the Cuyonon Palaweños, 
                            or simply Cuyonon. Because of the isle’s proximity to the Visayas, its language has 
                            similarities with the Visayan languages. Ratagnon, a language in Mindoro, also has many 
                            similarities with Cuyonon.     
                        </Text>
                        <Text style={{marginTop:30, marginLeft:15, marginRight:15, fontSize:14}}>
                            The Cuyonon Palaweños have mixed their old animist beliefs with Christianity to come up 
                            with a new set. They have retained their Cuyonon identity even as their society slowly 
                            blends with the modern world.    
                        </Text>
                        <Text style={{marginTop:40, marginLeft:0, fontSize:14}}>
                            - Daily activities and source of income   
                        </Text>
                        <Image style={{width:230, height:150, borderWidth:20, marginTop:15}} source={require('../image/Products/batak-product-1.jpg')}></Image>
                        <Image style={{width:230, height:150, borderWidth:20, marginTop:15, marginBottom:30}} source={require('../image/batak-3.png')}></Image>

                    </View>
                </ScrollView>
            </ImageBackground>
            </View>

            <View style={styles.pageStyle} key="5">
            <ImageBackground source={require('../image/mangyan-1.jpg')} style={styles.container}>
                
                <ScrollView style={styles.inner} scrollEventThrottle={16}>
                    
                <View style={styles.container}>
                        
                        <Text style={{fontSize:22, fontWeight:'100', paddingHorizontal:20, marginTop:40}}>
                            Mangyans               
                        </Text>

                        <Image style={{width:230, height:150, borderWidth:20, marginTop:25}} source={require('../image/mangyan-1.jpg')}></Image>

                        <Text style={{marginTop:30, marginLeft:15, marginRight:15, fontSize:14}}>
                            The Mangyans of Mindoro are well-known clans in the Philippines because they have the 
                            biggest populate. Comprising eight different Mangyan groups, they have a peaceful 
                            reputation, unlike the headhunting tribes of the North and warrior tribes from the South. 
                            While some has already converted into Christianity, there’s still a large percentage of 
                            those who practice animistic religious beliefs.   
                        </Text>
                        <Image style={{width:230, height:150, borderWidth:20, marginTop:15}} source={require('../image/mangyan-3.jpg')}></Image>
                        <Text style={{marginTop:40, marginLeft:0, fontSize:14}}>
                            - Daily activities and source of income   
                        </Text>
                        <Image style={{width:230, height:150, borderWidth:20, marginTop:15}} source={require('../image/Products/mangyan-product-1.jpg')}></Image>
                        <Image style={{width:230, height:150, borderWidth:20, marginTop:15, marginBottom:30}} source={require('../image/mangyan-2.jpg')}></Image>

                    </View>
                </ScrollView>
            </ImageBackground>
            </View>

            <View style={styles.pageStyle} key="6">
            <ImageBackground source={require('../image/aeta-1.jpg')} style={styles.container}>
                
                <ScrollView style={styles.inner} scrollEventThrottle={16}>
                    
                <View style={styles.container}>
                        
                        <Text style={{fontSize:22, fontWeight:'100', paddingHorizontal:20, marginTop:40}}>
                            Aetas or Negritos            
                        </Text>

                        <Image style={{width:230, height:150, borderWidth:20, marginTop:25}} source={require('../image/aeta-1.jpg')}></Image>

                        <Text style={{marginTop:30, marginLeft:15, marginRight:15, fontSize:14}}>
                            The Aetas (or Agta or Ayta) are one of the earliest known inhabitnats of the Philippines who 
                            are now living in scattered mountainous areas of the country. They were called by the Spanish 
                            colonizers as the ‘Negritos’ because of their dark to dark-brown skin. They are nomadic but 
                            are very skilled in weaving and planting, and Aeta women are considered experts in herbal 
                            medicine.   
                        </Text>
                        <Image style={{width:230, height:350, borderWidth:20, marginTop:15}} source={require('../image/aeta-2.jpg')}></Image>
                        <Text style={{marginTop:30, marginLeft:15, marginRight:15, fontSize:14}}>
                            Nowadays, rare is the Ayta wearing traditional clothing: the bahag (loincloth) for men and 
                            wraparound skirts for women. They now use urban attire. Ayta women are skilled in weaving, 
                            plaiting, and producing household containers, rattan hammocks, and winnowing baskets of 
                            excellent quality. Nomadic Ayta build temporary settlements made of indigenous materials, 
                            such as forked sticks, palm or banana leaves, cogon, and bamboo.   
                        </Text>
                        <Image style={{width:230, height:150, borderWidth:20, marginTop:15}} source={require('../image/aeta-3.jpg')}></Image>
                        <Text style={{marginTop:40, marginLeft:0, fontSize:14}}>
                            - Daily activities and source of income   
                        </Text>
                        <Image style={{width:230, height:150, borderWidth:20, marginTop:15}} source={require('../image/Products/aeta-products-1.jpg')}></Image>
                        <Image style={{width:230, height:150, borderWidth:20, marginTop:15, marginBottom:30}} source={require('../image/Products/aeta-products-2.jpg')}></Image>

                    </View>
                </ScrollView>
            </ImageBackground>
            </View>

          </ViewPagerAndroid>
        );
      }
}


const styles = StyleSheet.create ({
  
    viewPager: {
      flex: 1
    },
    pageStyle: {
      alignItems: 'center',
      padding: 0,
    },
    container: {
        flex:1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    inner: {
        width: '100%',
        height: '99%',
        backgroundColor: 'rgba(255,255,255, 0.7)'
    },
    head:{
        width:'100%',
        height:'15%'
    }
  })

export default Tribes;