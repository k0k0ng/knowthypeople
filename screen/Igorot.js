import React, { Component } from "react";
import{ View, Text, StyleSheet, ImageBackground, ScrollView, Image} from "react-native";

import {createBottomTabNavigator} from 'react-navigation'
import { Header,Left,Icon } from "native-base";

class Igorot extends Component{
    
    static navigationOptions = {
        drawerIcon: ({ tintColor }) => (
            <Icon name="people" style={{fontSize:24, color: tintColor}} />
        )
    }

    render(){
        return(
            <ImageBackground source={require('../image/igorot-3.jpg')} style={styles.container}>
                <View style={styles.head}>
                    <Header style={{height:78, backgroundColor:"#99ffff"}}>
                        <Left>
                            <Icon style={{position:'absolute', left:-80}} name="menu" onPress={()=>this.props.navigation.openDrawer()}/>
                        </Left>
                    </Header>
                </View>
                <ScrollView style={styles.inner} scrollEventThrottle={16}>
                    
                    <View style={styles.container}>
                        
                        <Text style={{fontSize:22, fontWeight:'700', paddingHorizontal:20}}>
                            Northern Igorots      
                        </Text>
                        <Text style={{fontSize:18, fontWeight:'400', paddingHorizontal:20}}>
                            of the      
                        </Text>
                        <Text style={{fontSize:22, fontWeight:'700', paddingHorizontal:20}}>
                            Philippine's               
                        </Text>



                        <Text style={{marginTop:30, marginLeft:15, marginRight:15, marginStart:25, fontSize:14}}>
                            The Igorots, which comprises numerous tribes in the northern part of the country, are 
                            mostly residing in the mountain ranges of the Cordillera Region. They are popularly known 
                            for being rice cultivators. An assortment of the group called the Ifugaos built the Banaue 
                            Rice Terraces – frequently called the ‘eighth wonder of the world’. The ancestors of this 
                            indigenous tribe carved a system of irrigated rice terraces in the mountains of Ifugao more 
                            than 2,000 years ago.      
                        </Text>

                        <Image style={{width:230, height:150, borderWidth:20, marginTop:20}} source={require('../image/igorot-1.jpg')}></Image>

                        <Text style={{marginTop:30, marginLeft:15, marginRight:15, marginBottom:55, fontSize:14}}>
                            The Igorot tribe also includes the Bontoc, Ibaloi, Isneg, Kalinga, Kankanaey and Tinguian 
                            groups. Other tribes living in the north are Isnag from Apayao, Gaddang (found between 
                            Kalinga and Isabela provinces), and the Ilongots living within the east mountains of Luzon 
                            called the Sierra Madre and the Caraballo Mountains. The Ilongots are known for their 
                            intense aggressiveness and cultural conservatism.       
                        </Text>

                    </View>
                </ScrollView>
            </ImageBackground>
        );
    }
}

class Products extends Component{
    static navigationOptions = {
        drawerIcon: ({ tintColor }) => (
            <Icon name="people" style={{fontSize:24, color: tintColor}} />
        )
    }
    
    render(){
        return(
            <ImageBackground source={require('../image/Products/igorot-product-1.jpg')} style={styles.container}>
                <View style={styles.head}>
                    <Header style={{height:78, backgroundColor:"#99ffff"}}>
                        <Left>
                            <Icon style={{position:'absolute', left:-80}} name="menu" onPress={()=>this.props.navigation.openDrawer()}/>
                        </Left>
                    </Header>
                </View>
                <ScrollView style={styles.inner} scrollEventThrottle={16}>
                    
                    <View style={styles.container}>
                        
                        <Text style={{fontSize:22, fontWeight:'500', paddingHorizontal:20}}>
                            Northern Igorots      
                        </Text>
                        <Text style={{fontSize:22, fontWeight:'500', paddingHorizontal:20}}>
                            Product's               
                        </Text>

                        <Text style={{fontSize:14, paddingHorizontal:20, marginTop:30}}>- Jars</Text>
                        <Image style={{width:230, height:150, borderWidth:20, marginTop:10}} source={require('../image/Products/igorot-product-2.jpg')}></Image>

                        <Text style={{fontSize:14, paddingHorizontal:20, marginTop:25}}>- Weaving</Text>
                        <Image style={{width:230, height:150, borderWidth:20, marginTop:10}} source={require('../image/Products/igorot-product-3.jpg')}></Image>

                        <Text style={{fontSize:14, paddingHorizontal:20, marginTop:25}}>- Baskets</Text>
                        <Image style={{width:230, height:150, borderWidth:20, marginTop:10, marginBottom:40}} source={require('../image/Products/igorot-product-1.jpg')}></Image>

                    </View>
                </ScrollView>
            </ImageBackground>
        );
    }
} 


const HomeScreenTabNavigator = createBottomTabNavigator({
    Igorot: {screen: Igorot,
             navigationOptions:{
                 tabBarLabel: 'About',
                 tabBarIcon: ({ tintColor }) => (
                    <Icon name="people" style={{fontSize:24, color: tintColor}} />
                 )
             }},
    Products: {screen: Products,
        navigationOptions:{
            tabBarLabel: 'Products and Activities',
            tabBarIcon: ({ tintColor }) => (
               <Icon name="bicycle" style={{fontSize:24, color: tintColor}} />
            )
        }}
},{
    animationEnabled: true
});


const styles=StyleSheet.create({
    container: {
        flex:1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    inner: {
        width: '94%',
        height: '99%',
        backgroundColor: 'rgba(255,255,255, 0.7)',
        marginTop: 10
    },
    head:{
        width:'100%',
        height:'15%'
    }

});

export default HomeScreenTabNavigator;