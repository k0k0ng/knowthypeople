import React, { Component } from "react";
import{ View, Text, StyleSheet, ImageBackground, ScrollView, Image} from "react-native";

import {createBottomTabNavigator} from 'react-navigation'
import { Header,Left,Icon } from "native-base";

class Other_Tribes extends Component{
    render(){
        return(
            <ImageBackground source={require('../image/other.jpg')} style={styles.container}>
                <View style={styles.head}>
                    <Header style={{height:78}}>
                        <Left>
                            <Icon style={{position:'absolute', left:-80}} name="menu" onPress={()=>this.props.navigation.openDrawer()}/>
                        </Left>
                    </Header>
                </View>
                <ScrollView style={styles.inner} scrollEventThrottle={16}>
                    
                <View style={styles.container}>
                        
                        <Text style={{fontSize:22, fontWeight:'700', paddingHorizontal:20}}>
                            Other major Tribes            
                        </Text>
                        <Text style={{fontSize:18, fontWeight:'400', paddingHorizontal:20}}>
                            in the      
                        </Text>
                        <Text style={{fontSize:22, fontWeight:'700', paddingHorizontal:20}}>
                            Philippine's                 
                        </Text>

                        <Text style={{marginTop:30, marginLeft:15, marginRight:15, marginStart:25, fontSize:14}}>
                            Apart from the two main indigenous groups mentioned above, the following tribes have 
                            also kept their customs and traditions.     
                        </Text>

                        <Text style={{fontSize:17, fontWeight:'300', paddingHorizontal:20, marginTop:30}}>
                           Badjaos               
                        </Text>

                        <Image style={{width:230, height:150, borderWidth:20, marginTop:15}} source={require('../image/badjao-3.jpg')}></Image>

                        <Text style={{marginTop:30, marginLeft:15, marginRight:15, fontSize:14}}>
                            Originally from the islands of Sulu in Mindanao, they’re known as the sea tribes living 
                            on houseboats. They try to make ends meet by depending on the sea as divers, fishermen, 
                            and navigators. Because of conflicts in the region, the majority of them has migrated to 
                            neighboring countries such as Malaysia and Indonesia, whereas those who stayed in the 
                            Philippines moved to some areas in Luzon.    
                        </Text>

                        <Text style={{fontSize:17, fontWeight:'300', paddingHorizontal:20, marginTop:35}}>
                            Ati and Tumandok  
                        </Text>

                        <Image style={{width:230, height:150, borderWidth:20, marginTop:15}} source={require('../image/ati-2.jpg')}></Image>

                        <Text style={{marginTop:30, marginLeft:15, marginRight:15, fontSize:14}}>
                            One of the few clans in Visayas, the Ati and Tumandok tribes of Panay Island are the 
                            first to call the island their home. Genetically related to other indigenous groups in the 
                            country, they mostly resemble the Aetas or Negritos who are characterised by their dark skin. 
                            While some adopted Western religions, they still carry some animistic beliefs and rituals 
                            passed down by their ancestors.   
                        </Text>
                        <Text style={{marginTop:30, marginLeft:15, marginRight:15, fontSize:14}}>
                            Atis cultivate a variety of crops for their livelihood. Tobacco is bartered for the products 
                            of their Visayan neighbors. During September and October, they work at the sugar plantations 
                            of Christian landowners. Other means of subsistence are hunting, fishing, handicrafts, and 
                            bow-and-arrow making; working as household help and midwives; and practicing herbal medicine 
                            as herbolarios.  
                        </Text>

                        <Text style={{fontSize:17, fontWeight:'300', paddingHorizontal:20, marginTop:35}}>
                            Palawan Tribes                           
                        </Text>

                        <Image style={{width:230, height:150, borderWidth:20, marginTop:15}} source={require('../image/palawenos-1.jpg')}></Image>

                        <Text style={{marginTop:30, marginLeft:15, marginRight:15, fontSize:14}}>
                            Palawan is also home to various tribes such as the Batak, Palaweño, Palawano, and the 
                            Tagbanwa. Mostly living in mountains or lowland dwellings, some of these groups have also 
                            been included in the large Manobo tribe of the South. They have not totally embraced urban 
                            living, with the majority living in more rural settings.      
                        </Text>

                        <Text style={{fontSize:13, fontWeight:'200', paddingHorizontal:20, marginTop:35}}>
                            Batak                          
                        </Text>

                        <Image style={{width:230, height:150, borderWidth:20, marginTop:15}} source={require('../image/batak-3.png')}></Image>

                        <Text style={{marginTop:30, marginLeft:15, marginRight:15, fontSize:14}}>
                            In Palawan, the smallest and the most endangered of the three major ethnic groups is the 
                            Batak tribe. An old Cuyunon term, Batak means “mountain people”; they are classified as a 
                            Philippine Aeta group because of their physical characteristics. At present, they live in 
                            the rugged northeastern part of Palawan Island, close to the coastal villages of Babuyan, 
                            Tinitian, and Malcampo. Previously, they lived in several river valleys of Babuyan, Maoyon, 
                            Tanabag, Tarabanan, Laingogan, Tagnipa, Caramay, and Buayan. They speak a language called 
                            Batak or Binatak, although majority are bilingual, as they can speak both Batak and Tagbanua.
                            Bataks practice minimal shifting cultivation, alternating rice with cassava, tubers, and 
                            vegetables.     
                        </Text>

                        <Text style={{fontSize:13, fontWeight:'200', paddingHorizontal:20, marginTop:35}}>
                            Palaweños                          
                        </Text>

                        <Image style={{width:230, height:150, borderWidth:20, marginTop:15}} source={require('../image/palawenos-2.jpg')}></Image>

                        <Text style={{marginTop:30, marginLeft:15, marginRight:15, fontSize:14}}>
                            The island of Cuyo between Palawan and Panay islands is the home of the Cuyonon Palaweños, 
                            or simply Cuyonon. Because of the isle’s proximity to the Visayas, its language has 
                            similarities with the Visayan languages. Ratagnon, a language in Mindoro, also has many 
                            similarities with Cuyonon.     
                        </Text>
                        <Text style={{marginTop:30, marginLeft:15, marginRight:15, fontSize:14}}>
                            The Cuyonon Palaweños have mixed their old animist beliefs with Christianity to come up 
                            with a new set. They have retained their Cuyonon identity even as their society slowly 
                            blends with the modern world.    
                        </Text>
                        

                        <Text style={{fontSize:17, fontWeight:'300', paddingHorizontal:20, marginTop:35}}>
                            Mangyans                    
                        </Text>

                        <Image style={{width:230, height:150, borderWidth:20, marginTop:15}} source={require('../image/mangyan-1.jpg')}></Image>

                        <Text style={{marginTop:30, marginLeft:15, marginRight:15, fontSize:14}}>
                            The Mangyans of Mindoro are well-known clans in the Philippines because they have the 
                            biggest populace. Comprising eight different Mangyan groups, they have a peaceful 
                            reputation, unlike the headhunting tribes of the North and warrior tribes from the South. 
                            While some has already converted into Christianity, there’s still a large percentage of 
                            those who practice animistic religious beliefs.   
                        </Text>


                        <Text style={{fontSize:17, fontWeight:'300', paddingHorizontal:20, marginTop:35}}>
                            Aetas or Negritos                        
                        </Text>

                        <Image style={{width:230, height:150, borderWidth:20, marginTop:15}} source={require('../image/aeta-1.jpg')}></Image>

                        <Text style={{marginTop:30, marginLeft:15, marginRight:15, fontSize:14}}>
                            The Aetas (or Agta or Ayta) are one of the earliest known inhabitnats of the Philippines who 
                            are now living in scattered mountainous areas of the country. They were called by the Spanish 
                            colonizers as the ‘Negritos’ because of their dark to dark-brown skin. They are nomadic but 
                            are very skilled in weaving and plaiting, and Aeta women are considered experts in herbal 
                            medicine.   
                        </Text>

                        <Text style={{marginTop:40, marginLeft:15, marginRight:15,marginBottom:40, fontSize:14}}>
                            Today, the indigenous groups in the country remain in their original ancestral lands as they 
                            preserve their cultural practices and traditions. And while the Philippine government passed 
                            the Indigenous Peoples’ Rights Act of 1997, there are still issues left unsolved, including 
                            their right for inclusion.   
                        </Text>

                    </View>
                </ScrollView>
            </ImageBackground>
        );
    }
}

class Products extends Component{
    render(){
        return(
            <ImageBackground source={require('../image/Products/baskets.jpg')} style={styles.container}>
                <View style={styles.head}>
                    <Header style={{height:78}}>
                        <Left>
                            <Icon style={{position:'absolute', left:-80}} name="menu" onPress={()=>this.props.navigation.openDrawer()}/>
                        </Left>
                    </Header>
                </View>
                <ScrollView style={styles.inner} scrollEventThrottle={16}>
                    
                    <View style={styles.container}>
                        
                        <Text style={{fontSize:22, fontWeight:'500', paddingHorizontal:20}}>
                            Other tribes'      
                        </Text>
                        <Text style={{fontSize:22, fontWeight:'500', paddingHorizontal:20}}>
                            Product's and                
                        </Text>

                        <Text style={{fontSize:14, paddingHorizontal:20, marginTop:30}}>- Badjao fishermen</Text>
                        <Image style={{width:230, height:150, borderWidth:20, marginTop:10}} source={require('../image/Products/badjao-product-2.jpg')}></Image>
                        <Image style={{width:230, height:150, borderWidth:20, marginTop:10}} source={require('../image/Products/badjao-product-3.jpg')}></Image>

                        <Text style={{fontSize:14, paddingHorizontal:20, marginTop:30}}>- Ati farmers and hunters</Text>
                        <Image style={{width:230, height:150, borderWidth:20, marginTop:10}} source={require('../image/Products/ati-product-1.jpg')}></Image>
                        <Image style={{width:230, height:150, borderWidth:20, marginTop:10}} source={require('../image/Products/ati-product-2.jpg')}></Image>

                        <Text style={{fontSize:14, paddingHorizontal:20, marginTop:30}}>- Mangyan</Text>
                        <Image style={{width:230, height:150, borderWidth:20, marginTop:10}} source={require('../image/Products/mangyan-product-1.jpg')}></Image>
                        <Image style={{width:230, height:150, borderWidth:20, marginTop:10}} source={require('../image/Products/mangyan-product-2.jpg')}></Image>

                        <Text style={{fontSize:14, paddingHorizontal:20, marginTop:30}}>- Aeta</Text>
                        <Image style={{width:230, height:150, borderWidth:20, marginTop:10}} source={require('../image/Products/aeta-products-1.jpg')}></Image>
                        <Image style={{width:230, height:150, borderWidth:20, marginTop:10}} source={require('../image/Products/aeta-products-2.jpg')}></Image>

                    </View>
                </ScrollView>
            </ImageBackground>
        );
    }
} 


const HomeScreenTabNavigator = createBottomTabNavigator({
    Other_Tribes: {screen: Other_Tribes},
    Products: {screen: Products}
},{
    animationEnabled: true
});


const styles=StyleSheet.create({
    container: {
        flex:1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    inner: {
        width: '94%',
        height: '99%',
        backgroundColor: 'rgba(255,255,255, 0.7)',
        marginTop:10
    },
    head:{
        width:'100%',
        height:'15%'
    }

});

export default HomeScreenTabNavigator;