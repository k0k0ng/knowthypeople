import React from 'react';
import { StyleSheet, Text, View, SafeAreaView, ScrollView, Dimensions, Image} from 'react-native';

import {createDrawerNavigator, DrawerItems } from 'react-navigation'
import { Icon } from "native-base";

import HomeScreen from "./screen/HomeScreen"
import Igorot from "./screen/Igorot"
import Lumad from "./screen/Lumad"
import Tribes from "./screen/Tribes"



export default class App extends React.Component {
  render() {
    return (
      <AppNavigator/>
    );
  }
}

const CustomComponents = (props) => (
  <SafeAreaView style={{flex:1}}>
    <View style={{height:150, backgroundColor:'white', alignItems:'center', justifyContent:'center'}}>
      <Image source={require('./assets/logo.png')} style={{height:120, width:120, borderRadius:60, marginTop:20}} />
    </View>
    <ScrollView>
      <DrawerItems {...props} />
    </ScrollView>
  </SafeAreaView>
)



const AppNavigator = createDrawerNavigator({
  Home  : { screen: HomeScreen},
  Igorot  : { screen: Igorot,
              navigationOptions:{
                title: "Igorots",
                drawerIcon: ({ tintColor }) => (
                  <Icon name="people" size={24} style={{ color: 'black' }} />
                ),
              }},
  Lumad  : { screen: Lumad,
              navigationOptions:{
                title: "Lumadas",
                drawerIcon: ({ tintColor }) => (
                  <Icon name="people" size={24} style={{ color: 'black' }} />
                ),
              }},
  Tribes: {screen: Tribes,
           navigationOptions:{
             title: "Other Tribes",
             drawerIcon: ({ tintColor }) => (
              <Icon name="people" size={24} style={{ color: 'black' }} />
            ),
           }}

},{
  contentComponent: CustomComponents,
  contentOptions: {
    activeTintColor: 'orange'
  }
})


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
